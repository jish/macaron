package main

import (
	"fmt"
	"html"
	"log"
	"net/http"
	"strconv"
)

func main() {
	http.HandleFunc("/favicon.ico", faviconHandler)
	http.HandleFunc("/", indexHandler)

	port := ":8080"
	fmt.Printf("Listening on %s\n", port)
	log.Fatal(http.ListenAndServe(port, nil))
}

// Chrome sends in an extra request to `/favicon.ico`
// We have this handler here as a no-op to avoid processing the favicon request
func faviconHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("Processing %q\n", html.EscapeString(r.URL.Path))
	// TODO What should we do with requests to `/favicon.ico`?
	// Send a file back?
	// A 404?
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("Processing %q\n", html.EscapeString(r.URL.Path))

	cookies := r.Cookies()
	cookie := getVisitsCookie(cookies)
	visits := incrementVisits(cookie, 1)

	http.SetCookie(w, cookie)

	fmt.Fprintln(w, buildResponse(visits))
}

func getVisitsCookie(cookies []*http.Cookie) *http.Cookie {
	for i := range cookies {
		cookie := cookies[i]
		if cookie.Name == "visits" {
			return cookie
		}
	}

	return &http.Cookie{
		Name:  "visits",
		Value: "0",
	}
}

func incrementVisits(cookie *http.Cookie, count int) int {
	visits, err := strconv.Atoi(cookie.Value)
	if err != nil {
		log.Fatal("error: %q\n", err)
	}

	visits += count
	cookie.Value = strconv.Itoa(visits)

	return visits
}

func buildResponse(visits int) string {
	response := "<html><head></head><body><h1>Macaron</h1>"

	if visits > 1 {
		response += "<p>Cookies: yes, Visits: " + strconv.Itoa(visits) + "</p>"
	} else {
		response += "<p>Cookies: ?</p>"
	}

	response += "<noscript>JavaScript: no</noscript>"
	response += "<script>" +
		"n=document.createElement('p');" +
		"n.innerText='JavaScript: yes';" +
		"b=document.getElementsByTagName('body')[0];" +
		"b.appendChild(n);"
	response += "</script>"

	response += "</body></html>"

	return response
}
